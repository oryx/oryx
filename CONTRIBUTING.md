# How to Contribute

Oryx accepts contributions via Gitlab pull requests. Here we outline some of
the conventions on development workflow, commit message formatting, contact
points and other resources to make it easier to get your contribution
accepted.

The components within Oryx are licensed as follows:

- Recipes and build scripts: [MIT License](https://opensource.org/licenses/MIT)

- Documentation: [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/)

- Patches to third-party software: Distributed under the same license as the
  software being patched

By contributing to this project you agree to the
[Developer Certificate of Origin (DCO)](#developer-certificate-of-origin).
This document was created by the Linux Kernel community and is a simple
statement that you, as a contributor, have the legal right to make the
contribution. See the DCO text at the end of this file for details.

## Reporting Issues

Bugs and feature requests may be reported via our
[issue tracker](https://gitlab.com/oryx/oryx/issues).

Please do not use our public issue tracker to report security bugs or other
sensitive issues, instead please report these by email to
security@oryx-linux.org.

## Contributing Code

- Fork the repository on Gitlab
- Read the [Yocto Project Community Guidelines](https://wiki.yoctoproject.org/wiki/Community_Guidelines)
- Play with the project, submit bugs, submit patches!

### Contribution Flow

This is an outline of what a contributor's workflow looks like:

- Create a topic branch from where you want to base your work (usually master).
- Make commits of logical units.
- Make sure your commit messages are in the proper format (see below).
- Push your changes to a topic branch in your fork of the repository.
- Submit a pull request to the original repository.

Thanks for your contributions!

### Format of the Commit Message

See the [OpenEmbedded patch guidelines](https://www.openembedded.org/wiki/Commit_Patch_Message_Guidelines).

See the [OpenEmbedded styleguide](https://www.openembedded.org/wiki/Styleguide)

### Developer Certificate of Origin

    Developer Certificate of Origin
    Version 1.1

    Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
    660 York Street, Suite 102,
    San Francisco, CA 94110 USA

    Everyone is permitted to copy and distribute verbatim copies of this
    license document, but changing it is not allowed.


    Developer's Certificate of Origin 1.1

    By making a contribution to this project, I certify that:

    (a) The contribution was created in whole or in part by me and I
        have the right to submit it under the open source license
        indicated in the file; or

    (b) The contribution is based upon previous work that, to the best
        of my knowledge, is covered under an appropriate open source
        license and I have the right under that license to submit that
        work with modifications, whether created in whole or in part
        by me, under the same open source license (unless I am
        permitted to submit under a different license), as indicated
        in the file; or

    (c) The contribution was provided directly to me by some other
        person who certified (a), (b) or (c) and I have not modified
        it.

    (d) I understand and agree that this project and the contribution
        are public and that a record of the contribution (including all
        personal information I submit with it, including my sign-off) is
        maintained indefinitely and may be redistributed consistent with
        this project or the open source license(s) involved.
