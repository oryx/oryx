==========
Oryx Linux
==========

Oryx Linux is a Linux® distribution targeted at embedded applications and based
on the work of `The Yocto Project <https://www.yoctoproject.org/>`_ and
`OpenEmbedded <https://www.openembedded.org/>`_. It incorporates a
lightweight container runtime engine to bring the benefits of
containerization to the embedded sector without disrupting existing developer
workflows. For further details, see the :ref:`motivation` section of this
documentation.

This documentation covers the |version| version of Oryx Linux.

The latest version of the documentation is available online at the
`Oryx ReadTheDocs site <https://oryx.readthedocs.io/en/latest/>`_.

.. toctree::
   :maxdepth: 2

   intro
   release-history
   getting-started
   using-oryxcmd
   building-images
   mender-integration
   contributing
